import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY

from .models import Location


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {"query": city + " " + state}
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)

    content = response.content
    parsed_json = json.loads(content)
    # print(parsed_json(["photos"][0]["src"]["original"]))
    picture = parsed_json["photos"][0]["src"]["original"]

    return {"picture_url": picture}


def get_lat_long(location):
    # return lat and long of specified location using the OWM API
    base_url = "https://api.openweathermap.org/geo/1.0/direct"
    params = {
        "q": f"{location.city},{location.state.abbreviation},USA",
        "appid": OPEN_WEATHER_API_KEY,
    }
    response = requests.get(base_url, params=params)
    parsed_json = json.loads(response.content)
    return {
        "latitude": parsed_json[0]["lat"],
        "longitude": parsed_json[0]["lon"],
    }


def get_weather_data(location):
    """
    Returns current weather data for the specified locatoin
    using the OWM API
    """

    # get lat and long for the location
    lat_long = get_lat_long(location)
    # get weather data from API
    base_url = "https://api.openweathermap.org/data/2.5/weather"
    params = {
        "lat": lat_long["latitude"],
        "lon": lat_long["longitude"],
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    response = requests.get(base_url, params=params)
    # Parse the response object
    parsed_json = json.loads(response.content)
    # create a new dict
    weather_data = {
        "temp": parsed_json["main"]["temp"],
        "description": parsed_json["weather"][0]["description"],
    }

    # add "temp" and "description" properties in dict

    return weather_data
